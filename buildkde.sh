#!/bin/sh

. /etc/makepkg.conf
export ICECC_VERSION="/var/icecream/native-icecream.tar.gz,i686:/var/icecream/x86_64-cross-icecream.tar.gz"
export PATH=/opt/icecream/bin:$PATH
#export PATH=/usr/lib/distcc/bin:$PATH
export TMPDIR=/dev/shm

LOGFILE="${HOME}/buildkde.log"

# Directory under which the directories containing PKGBUILD files are stored
BASEDIR="${HOME}/kde-git-pkgbuilds"

#List of packages to install.  Order matters (because of dependencies).

#qtwebkit-git
PKGLIST="qt-git
automoc-git
attica-git
polkit-qt-1-git
soprano-git
akonadi-git
libstreamanalyzer-git
libstreams-git
strigiclient-git
strigidaemon-git
strigiutils-git
phonon-git
shared-desktop-ontologies
kdelibs-git
kdepimlibs-git
grantlee-git
konsole-git
kde-baseapps-git
kde-workspace-git
kde-runtime-git
kate-git
kdepim-git
kdepim-runtime-git
smokegen-git
smokeqt-git
smokekde-git
kross-interpreters-git
qtruby-git
korundum-git
pykde4-git
perlqt-git
perlkde-git
qyoto-git
kimono-git
kdegames-svn
libksane-git
libkexiv2-git
libkdcraw-git
libkipi-git
ksaneplugin-git
kolourpaint-git
ksnapshot-git
gwenview-git
kruler-git
svgpart-git
kcolorchooser-git
kgamma-git
kdegraphics-strigi-analyzer-git
kdegraphics-thumbnailers-git
kamera-git
okular-git
kdeartwork-svn
kdeplasma-addons-git
kdemultimedia-svn
kdenetwork-svn
kdesdk-svn
ark-git
filelight-git
kcalc-git
kcharselect-git
kdf-git
kfloppy-git
kgpg-git
printer-applet-git
kremotecontrol-git
ktimer-git
kwallet-git
superkaramba-git
sweeper-git
koffice-git
koffice-plugins-git
kdeadmin-svn
extragear-base-svn
extragear-graphics-svn
extragear-multimedia-svn
k3b-git
amarok-git
extragear-network-svn
extragear-pim-svn
kpilot-svn
kdevplatform-git
kdevelop-git
kdevelop-pg-qt-git
kdev-php-git
quanta-git
hotstuff-svn
libkface-git
marble-git
libkmap-git
libkdcraw-git
libkipi-git
kipi-plugins-git
libkgeomap-git
digikam-git
"
#playground-libs
#playground-base-plasma
#playground-office

CLEAN_BUILD="false"
CLEAN_SOURCES="false"
CLEAN_AFTER="false"
for arg 
	do case "$arg" in 
	    --package) shift; PKGLIST="$1" ;; 
		--help) shift; echo "Syntax: buildkde.sh [--help] [--cleanbuild] [--cleansources] [--fresh-build] [--package pkg-name]"; exit;
	esac
done

rm -f ${LOGFILE} #Clear log before each build attempt

#Install packages in the list in order
for d in ${PKGLIST};
do
	#Get the name of the package
	PKG=`basename $d`;

	echo "Building ${PKG}"
	echo "BuildScript: Building ${PKG}" >> ${LOGFILE}

	#Change directories to the package source directory
	#cd $d
	cd $BASEDIR/$d

	#Remove the old packages from the packages directory
	rm -f ${PKGDEST}/${PKG}*.pkg.tar.*
	rm -f ${BASEDIR}/$d/${PKG}*.pkg.tar.*

	#Build the package and log to mpkg.log
	makepkg 2>&1 >> $LOGFILE

	#Using sudo, install the package
	sudo pacman -Uf --noconfirm ${PKGDEST}/${PKG}*.pkg.tar.*

	#And add it to the "custom" repository.
	sudo repo-add -s ${PKGDEST}/custom.db.tar.gz ${PKGDEST}/${PKG}*.pkg.tar.*
done;
