# Maintainer: Eth-OS @ http://eth-os.org (AGPLv3)

pkgname=qt-git
_pkgname=qt
pkgver=20111121
pkgrel=1
pkgdesc="The Qt Gui Toolkit (KDE Copy)"
url="http://www.qtsoftware.com/"
license=(GPL)
groups=(kde-svn-all kde-svn)

arch=(i686 x86_64)
license=(GPL3 LGPL)

depends=(
  libpng libjpeg libxi libxcursor libxinerama mesa fontconfig libxrandr
  #dbus glib2 libcups libxml2 libmysqlclient postgresql-libs libmng libtiff
  dbus libcups libxml2 libmysqlclient postgresql-libs libmng libtiff
  zlib sqlite3 libxrender
)

optdepends=(postgresql-libs libmysqlclient)
makedepends=(inputproto cups libxfixes pkgconfig make git)
conflicts=("qt" "qt4")
provides=("qt=$pkgver" "qt4=$pkgver" qtwebkit)
options=(!libtool)

_gitroot="git://gitorious.org/qt/$_pkgname"
_gitname="$_pkgname"

source=(
  assistant.desktop
  designer.desktop
  linguist.desktop
  qtconfig.desktop
)

build()
{
  unset QMAKESPEC
  export QT4DIR=$SRCDEST/$_pkgname
  export PATH=${QT4DIR}/bin:${PATH}
  export LD_LIBRARY_PATH=${QT4DIR}/lib:${LD_LIBRARY_PATH}

  getsource

  cd $SRCDEST/$_pkgname

  sed -i "s|-O2|$CXXFLAGS|" mkspecs/common/g++.conf

  ./configure \
    -debug \
    -fast \
    -no-separate-debug-info \
    -system-libpng -system-libjpeg -system-zlib \
    -dbus -webkit \
    --reduce-exports \
    -plugin-sql-{psql,mysql,sqlite,odbc} \
    -nomake examples \
    -nomake demos \
    -nomake docs \
    -opensource \
    -confirm-license \
    -silent \
    -prefix /usr \
    -sysconfdir /etc \
    -plugindir /usr/lib/qt/plugins \
    -translationdir /usr/share/qt/translations \
    -datadir /usr/share/qt \
    -docdir /usr/share/doc/qt \
    -examplesdir /usr/share/doc/qt/examples \
    -demosdir /usr/share/doc/qt/demos \
    -system-sqlite \
    -no-phonon \
    -no-glib \
    -optimized-qmake \
    -reduce-relocations  || return 1

  make ${MAKEOPTS} || return 1
  make INSTALL_ROOT=$pkgdir install || return 1

  install -D -m644 tools/assistant/tools/assistant/images/assistant.png $pkgdir/usr/share/pixmaps/assistant.png
  install -D -m644 tools/linguist/linguist/images/appicon.png $pkgdir/usr/share/pixmaps/linguist.png
  install -D -m644 tools/designer/src/designer/images/designer.png $pkgdir/usr/share/pixmaps/designer.png
  install -D -m644 src/gui/dialogs/images/qtlogo-64.png $pkgdir/usr/share/pixmaps/qtlogo.png
  install -d $pkgdir/usr/share/applications
  install -m644 $srcdir/{linguist,designer,assistant,qtconfig}.desktop $pkgdir/usr/share/applications/

  find $pkgdir/usr/lib -type f -name '*prl' -print -exec sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" {} \;
  sed -i -e "s|-L$SRCDEST/${_pkgfqn}/lib||g" $pkgdir/usr/lib/pkgconfig/*.pc
  sed -i -e "s|$SRCDEST/${_pkgfqn}/bin/moc|/usr/bin/moc|g" $pkgdir/usr/lib/pkgconfig/*.pc
  sed -i -e "s|$SRCDEST/${_pkgfqn}/bin/uic|/usr/bin/uic|g" $pkgdir/usr/lib/pkgconfig/*.pc
}

getsource()
{
  [ -d $SRCDEST ] && cd $SRCDEST || return 1
  msg "Connecting to git server...."
  if [ -d $_gitname ]; then
    cd $_gitname && git pull origin
  else
    git clone $_gitroot $_gitname
  fi

  msg "GIT checkout done or server timeout"
  msg "Starting make..."
  return 0
}

sha1sums=('f7ee569952876ac918d8892412293e57821d2714'
          '2bb0a30fba40e20cef6dac1e1401db463635d19e'
          'd73412dcd01ad48519ab9162523c08386d01858f'
          'c490473a00eb2128286f045088791ad754376bc4')
