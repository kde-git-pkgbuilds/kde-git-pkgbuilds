export MAKEOPTS="-j3"

getsvnsource()
{
  local_pkgname=$_pkgname
  [ -n $local_pkgname ] || local_pkgname=$pkgname
  [ -d $SRCDEST ] && cd $SRCDEST || return 1
  [ -n $pkgpath ] || pkgpath=trunk/$local_pkgname
  if [ -d KDE/$local_pkgname/.svn ]; then
	  cd KDE/$local_pkgname 
	  VERSION=`svn info | grep Revision | awk '{print \$2;}'`
      echo VERSION:$VERSION
	  svn up
  	  echo Changes:
	  cd $SRCDEST/KDE/$local_pkgname
	  svn log -r $VERSION:`svn info | grep Revision | awk '{print \$2;}'` || echo "No Changes"
  else
    svn co svn://anonsvn.kde.org/home/kde/$pkgpath KDE/$local_pkgname
    cd KDE/$local_pkgname
  fi
}

getgitsource()
{
   local_pkgname=$_pkgname
   [ -n $local_pkgname ] || local_pkgname=$pkgname
   [ -d $SRCDEST ] && cd $SRCDEST || return 1
   if [ -d KDE/$local_pkgname/.git ]; then
	   cd KDE/$local_pkgname
	   VERSION=`git describe` || VERSION=HEAD^
	   git pull
   	   echo Changes:
	   git log ${VERSION}..HEAD
   else
      git clone kde:$_pkgname.git KDE/$_pkgname
      cd KDE/$_pkgname
      VERSION=`git describe` || VERSION=HEAD^
   fi
}
