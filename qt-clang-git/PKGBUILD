# Maintainer: Eth-OS @ http://eth-os.org (AGPLv3)

pkgname=qt-clang-git
_pkgname=qt
pkgver=$(date -u +%Y%m%d%H)
pkgrel=1
pkgdesc="The Qt Gui Toolkit (KDE Copy)"
url="http://www.qtsoftware.com/"
license=(GPL)
groups=(kde-svn-all kde-svn)

arch=(i686 x86_64)
license=(GPL3 LGPL)

depends=(
  libpng libjpeg libxi libxcursor libxinerama mesa fontconfig libxrandr
  dbus glib2 libcups libxml2 libmysqlclient postgresql-libs libmng libtiff
  zlib sqlite3 libxrender
)

optdepends=(postgresql-libs libmysqlclient)
makedepends=(inputproto cups libxfixes pkgconfig make)
conflicts=(qt qt4)
provides=("qt=$pkgver" "qt4=$pkgver")
options=(!libtool)

source=(
  assistant.desktop
  designer.desktop
  linguist.desktop
  qtconfig.desktop
)

build()
{
  unset QMAKESPEC
  export QT4DIR=$SRCDEST/qt-clang
  export PATH=${QT4DIR}/bin:${PATH}
  export LD_LIBRARY_PATH=${QT4DIR}/lib:${LD_LIBRARY_PATH}

  getsource

  cd $SRCDEST/qt-clang

  #sed -i "s|-O2|$CXXFLAGS|" mkspecs/common/g++.conf
  #[ -d /usr/lib/ccache/bin ] && PATH=/usr/lib/ccache/bin:$PATH

  export CC=/usr/bin/clang 
  export CXX=/usr/bin/clang++ 
  ./configure \
    -opensource \
    -confirm-license \
    -fast \
    -system-libpng -system-libjpeg -system-zlib \
    -dbus -webkit \
    -silent \
    -prefix /usr \
    -sysconfdir /etc \
    -plugindir /usr/lib/qt/plugins \
    -translationdir /usr/share/qt/translations \
    -datadir /usr/share/qt \
    -docdir /usr/share/doc/qt \
    -examplesdir /usr/share/doc/qt/examples \
    -demosdir /usr/share/doc/qt/demos \
    -debug \
    -no-separate-debug-info \
    -plugin-sql-{psql,sqlite,odbc} \
    -system-sqlite \
    -platform unsupported/linux-clang \
    -nomake demos \
    -nomake examples \
    -nomake docs \
    -no-phonon \
    -optimized-qmake \
    -reduce-relocations  || return 1

#    -plugin-sql-{psql,mysql,sqlite,odbc} \
#    -qt-gif  \
  make || return 1
  make INSTALL_ROOT=$pkgdir install || return 1

  install -D -m644 tools/assistant/tools/assistant/images/assistant.png $pkgdir/usr/share/pixmaps/assistant.png
  install -D -m644 tools/linguist/linguist/images/appicon.png $pkgdir/usr/share/pixmaps/linguist.png
  install -D -m644 tools/designer/src/designer/images/designer.png $pkgdir/usr/share/pixmaps/designer.png
  install -D -m644 src/gui/dialogs/images/qtlogo-64.png $pkgdir/usr/share/pixmaps/qtlogo.png
  install -d $pkgdir/usr/share/applications
  install -m644 $srcdir/{linguist,designer,assistant,qtconfig}.desktop $pkgdir/usr/share/applications/

  find $pkgdir/usr/lib -type f -name '*prl' -print -exec sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" {} \;
  sed -i -e "s|-L$SRCDEST/${_pkgfqn}/lib||g" $pkgdir/usr/lib/pkgconfig/*.pc
  sed -i -e "s|$SRCDEST/${_pkgfqn}/bin/moc|/usr/bin/moc|g" $pkgdir/usr/lib/pkgconfig/*.pc
  sed -i -e "s|$SRCDEST/${_pkgfqn}/bin/uic|/usr/bin/uic|g" $pkgdir/usr/lib/pkgconfig/*.pc
}

getsource()
{
  [ -d $SRCDEST ] && cd $SRCDEST || return 1
  if [ -d qt-clang/.git ]; then
    cd qt-clang && git pull origin master
  else
    git clone git://gitorious.org/qt/qt.git qt-clang
  fi
  #rsync -a --delete $SRCDEST/$_pkgname/ $srcdir/$_pkgname/
  return 0
}

sha1sums=('f7ee569952876ac918d8892412293e57821d2714'
          '2bb0a30fba40e20cef6dac1e1401db463635d19e'
          'd73412dcd01ad48519ab9162523c08386d01858f'
          'c490473a00eb2128286f045088791ad754.86bc4')
